alumnos = [
  { nombre: 'mariana', edad: 22, comuna: 'macul', genero: 'f' },
  { nombre: 'gladys', edad: 25, comuna: 'linares', genero: 'f' },
  { nombre: 'javier', edad: 30, comuna: 'maipu', genero: 'm' },
  { nombre: 'eduardo', edad: 32, comuna: 'san miguel', genero: 'm' }
]

option = 0
while option != 10
  # --------------------- Main menu --------------------------- #
  puts
  puts '#------ Escoge una opción: ------ #'
  puts 'Opcion 1: Ingresar alumno.-'
  puts 'Opcion 2: Editar datos alumno.-'
  puts 'Opcion 3: Eliminar alumno.-'
  puts 'Opcion 4: Ver cantidad alumnos.-'
  puts 'Opcion 5: Ver comunas alumnos.-'
  puts 'Opcion 6: Alumnos entre 20 y 25 años.-'
  puts 'Opcion 7: Suma edades de alumnos.-'
  puts 'Opcion 8: Promedio edades de alumnos.-'
  puts 'Opcion 9: Genero de alumnos.-'
  puts 'Opcion 10: Salir.-'

  option = 0
  while option <= 0 || option > 10
    puts 'Opción:'
    option = gets.chomp.to_i
    puts
  end

  # --------------------- Option 1 --------------------------- #
  def enter_student(array)
    puts 'Ingrese alumno (nombre, edad, comuna, genero):'
    new_student = gets.chomp.downcase.split(', ')
    hash = {}
    hash[:nombre] = new_student[0]
    hash[:edad] = new_student[1].to_i
    hash[:comuna] = new_student[2]
    hash[:genero] = new_student[3]
    puts array.push(hash)
    puts 'Alumno Ingresado!'
  end

  # --------------------- Option 2 --------------------------- #
  def edit_student(array)
    puts 'Ingrese el nombre del alumno:'
    name = gets.chomp.downcase
    array.each do |i|
      if name == i[:nombre]
        print 'Nuevo Nombre: '
        i[:nombre] = gets.chomp
        print 'Nueva edad: '
        i[:edad] = gets.chomp.to_i
        print 'Nueva comuna: '
        i[:comuna] = gets.chomp
        print 'Nuevo género: '
        i[:genero] = gets.chomp
      end
    end
    print array
    puts
  end

  # --------------------- Option 3 --------------------------- #
  def delete_student(array)
    puts 'Ingrese nombre de alumno a eliminar:'
    name = gets.chomp.downcase
    array.each { |e| array.delete(e) if e[:nombre] == name }
    print array
    puts
  end

  # --------------------- Option 4 --------------------------- #
  def quantity_students(array)
    puts "Hay un total de #{array.length} alumnos"
  end

  # --------------------- Option 5 --------------------------- #
  def see_comunas(array)
    puts 'Listado de comunas:'
    comunas = array.group_by { |e| e[:comuna] }
    comunas.each_key { |key| puts "- #{key} " }
    puts
  end

  # --------------------- Option 6 --------------------------- #
  def between_20_25_years(array)
    puts 'Alumnos entre 20 y 25 años:'
    alumnos = array.select { |e| e[:edad] >= 20 && e[:edad] <= 25 }
    alumnos.each { |e| puts "- #{e[:nombre]} " }
    puts
  end

  # --------------------- Option 7 --------------------------- #
  def sum_years(array)
    puts 'Suma de todas las edades de alumnos:'
    suma = array.inject(0) { |sum, x| sum + x[:edad] }
    puts suma
  end

  # --------------------- Option 8 --------------------------- #
  def average_years(array)
    puts 'Promedio de edades de todos los alumnos:'
    suma = array.inject(0) { |sum, x| sum + x[:edad] }
    average = suma / array.length.to_f
    puts average.round(2) # round(2): con 2 decimales
  end

  # --------------------- Option 9 --------------------------- #
  def genere_students(array)
    puts 'Listado de alumnos por género:'
    students = array.group_by { |e| e[:genero] }
    students.each do |key, value|
      puts "Género: #{key}"
      value.each { |e| puts "- #{e[:nombre]}"}
    end
  end

  # --------------------- Option 10 --------------------------- #
  def exit_program
    puts 'Saliendo...'
  end

  case option
  when 1 then enter_student(alumnos)
  when 2 then edit_student(alumnos)
  when 3 then delete_student(alumnos)
  when 4 then quantity_students(alumnos)
  when 5 then see_comunas(alumnos)
  when 6 then between_20_25_years(alumnos)
  when 7 then sum_years(alumnos)
  when 8 then average_years(alumnos)
  when 9 then genere_students(alumnos)
  when 10 then exit_program
  end
end