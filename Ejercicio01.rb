# Dado el array:
a = [1, 2, 3, 9, 1, 4, 5, 2, 3, 6, 6]

# ---------------------------------------------------------------------------
# 1. Utilizando *map* generar un nuevo arreglo con cada valor aumentado en 1.
array_1 = a.map { |e| e + 1 }
print array_1
puts

# ---------------------------------------------------------------------------
# 2. Utilizando *map* generar un nuevo arreglo que contenga todos los valores
# convertidos a *float*.
array_2 = a.map { |e|  e.to_f }
print array_2
puts

# -----------------------------------------------------------------------------
# 3. Utilizando *map* generar un nuevo arreglo que contenga todos los valores
# convertidos a *string*.
array_3 = a.map { |e| e.to_s }
print array_3
puts

# -----------------------------------------------------------------------------
# 4. Utilizando *reject* descartar todos los elementos menores a 5 en el array.
array_4 = a.reject { |e| e < 5 }
print array_4
puts

# -----------------------------------------------------------------------------
# 5. Utilizando *select* descartar todos los elementos mayores a 5 en el array.
array_5 = a.select { |e| e < 5 }
print array_5
puts

# -----------------------------------------------------------------------------
# 6. Utilizando *inject* obtener la suma de todos los elementos del array.
suma_array = a.inject(0) { |sum, e| sum + e }
print suma_array
puts

# -----------------------------------------------------------------------------
# 7. Utilizando *group_by* agrupar todos los números por paridad (si son pares,
# es un grupo, si son impares es otro grupo).
array_6 = a.group_by { |e| e.even? }
print array_6
puts

# -----------------------------------------------------------------------------
# 8. Utilizando *group_by* agrupar todos los números mayores y menores que 6.
array_7 = a.group_by { |e| e < 6 }
print array_7
