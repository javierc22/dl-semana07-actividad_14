#Dado el arreglo
nombres = ["Violeta", "Andino", "Clemente", "Javiera", "Paula", "Pia", "Ray"]

#-----------------------------------------------------------------------------
# 1. Extraer todos los elementos que excedan mas de 5 caracteres utilizando
# el método *.select*.
array_1 = nombres.select { |e| e.length > 5 }
print array_1
puts

#-----------------------------------------------------------------------------
# 2. Utilizando *.map* crear una arreglo con los nombres en minúscula.
array_2 = nombres.map { |e| e.downcase }
print array_2
puts

#-----------------------------------------------------------------------------
# 3. Utilizando *.select*, crear un arreglo con todos los nombres que empiecen con P.
array_3 = nombres.select { |e| e[0] == 'P' }
print array_3
puts

#-----------------------------------------------------------------------------
# 4. Utilizando *.map* crear un arreglo único con la cantidad de letras que
# tiene cada nombre.
array_4 = nombres.map { |e| e.length }
print array_4
puts

#-----------------------------------------------------------------------------
# 5. Utilizando *.map* y *.gsub* eliminar las vocales de todos los nombres.
array_5 = nombres.map { |e| e.gsub(/[aeiou]/, '_') }
print array_5